package be.kdg.gedistribueerde.chat.client;

import be.kdg.gedistribueerde.chat.TextReceiver;

import java.net.ConnectException;

public class ChatClient {
    private ChatServerStub chatServerStub;
    public TextReceiver textReceiver;
    private String name;
    String address = "127.0.0.1";

    public ChatClient(String name, ChatServerStub chatServerStub) {
        this.chatServerStub = chatServerStub;
        this.name = name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void send(String message) {
        this.chatServerStub.send(message, this.name);
    }

    public void receive(String message) {
        if (textReceiver == null) return;
        textReceiver.receive(message);
    }

    public void setTextReceiver(TextReceiver textReceiver) {
        this.textReceiver = textReceiver;
    }

    public void unregister() {

    }

    public void register(int port) {
        try {
            String info = chatServerStub.connect(port, address);
            textReceiver.receive(info);
        } catch (ConnectException e) {
            textReceiver.receive("You are NOT connected to " + address + ":" + port + "\nplease try another port");
        }
    }
}
