package be.kdg.gedistribueerde.chat.client;

import be.kdg.gedistribueerde.chat.communication.MessageManager;
import be.kdg.gedistribueerde.chat.communication.MethodCallMessage;
import be.kdg.gedistribueerde.chat.communication.NetworkAddress;

import java.net.ConnectException;


public class ChatServerStub {
    private MessageManager messageManager;
    private NetworkAddress serverAdress;

    public ChatServerStub(MessageManager messageManager) {
        this.messageManager = messageManager;
    }

    public String connect(int port, String address) throws ConnectException {
        this.serverAdress = new NetworkAddress(address, port);
        System.out.println("connecting to :" + serverAdress.toString());
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "connect");
        messageManager.send(methodCallMessage, serverAdress);
        return checkConnectionReply();
    }

    public void send(String message,String name) {
        if (this.serverAdress == null) {
            System.out.println("u bent niet geconnecteerd");
            // textReceiver.receive("u bent niet geconnecteerd met de server");
        } else {
            try {
                System.out.println("messege :" + message + " word gestuurd naar :" + serverAdress.toString());
                MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "message");
                methodCallMessage.setParameter("message", message);
                methodCallMessage.setParameter("name", name);
                messageManager.send(methodCallMessage, serverAdress);
            } catch (ConnectException e) {
                e.printStackTrace();
            }
        }
    }

    private String checkConnectionReply() {
        String status = "";
        String result = "";
        while (!"Ok".equals(status)) {
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())) {
                continue;
            }
            status = reply.getParameter("status");
            result = reply.getParameter("result");
        }
        return result;
    }
}
