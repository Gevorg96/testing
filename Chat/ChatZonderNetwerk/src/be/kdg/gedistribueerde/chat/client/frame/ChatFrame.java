package be.kdg.gedistribueerde.chat.client.frame;

import be.kdg.gedistribueerde.chat.TextReceiver;
import be.kdg.gedistribueerde.chat.client.ChatClient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ChatFrame extends JFrame implements TextReceiver {
    private JLabel nameLabel;
    private JTextArea history;
    private JTextField messageField;
    private JButton sendButton;
    private JButton exitButton;
    private ChatClient chatClient;
    private JTextField serverPort;
    private JTextField txtName;
    private JButton connectButton;

    public ChatFrame(ChatClient chatClient) {
        this.chatClient = chatClient;
        chatClient.setTextReceiver(this);
        String name = chatClient.getName();
        setTitle("Chat: " + name);
        createComponents(name);
        layoutComponents();
        addListeners();
        setSize(300, 300);
        setVisible(true);
    }

    private void createComponents(String name) {
        this.nameLabel = new JLabel("Username :");
        this.serverPort = new JTextField("666");
        this.txtName = new JTextField("NO NAME");
        this.history = new JTextArea();
        this.history.setEditable(false);
        this.messageField = new JTextField();
        this.connectButton = new JButton("connect");
        this.sendButton = new JButton("send");
        this.exitButton = new JButton("exit");
    }

    private void layoutComponents() {
        JComponent portPanel = new JPanel(new BorderLayout());
        portPanel.add(serverPort, BorderLayout.CENTER);
        portPanel.add(connectButton, BorderLayout.EAST);
        portPanel.add(new JLabel("port: "), BorderLayout.WEST);

        JPanel namePanel = new JPanel(new BorderLayout());
        namePanel.add(nameLabel, BorderLayout.WEST);
        namePanel.add(txtName, BorderLayout.CENTER);

        JPanel topPanel = new JPanel(new BorderLayout());
        topPanel.add(portPanel, BorderLayout.AFTER_LAST_LINE);
        topPanel.add(namePanel, BorderLayout.BEFORE_FIRST_LINE);

        JScrollPane historyPane = new JScrollPane(history);
        JPanel bottomPanel = new JPanel(new BorderLayout());
        JPanel inputPanel = new JPanel(new BorderLayout());
        inputPanel.add(messageField, BorderLayout.CENTER);
        sendButton.setDefaultCapable(true);
        inputPanel.add(sendButton, BorderLayout.EAST);
        bottomPanel.add(inputPanel, BorderLayout.NORTH);
        JPanel exitPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        exitPanel.add(exitButton);
        bottomPanel.add(exitPanel, BorderLayout.SOUTH);
        Container contentPane = getContentPane();
        contentPane.add(topPanel, BorderLayout.NORTH);
        contentPane.add(historyPane, BorderLayout.CENTER);
        contentPane.add(bottomPanel, BorderLayout.SOUTH);
    }

    private void addListeners() {
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                stop();
            }
        });
        sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                send();
            }
        });
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                exit();
            }
        });
        messageField.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                send();
            }
        });
        connectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent actionEvent) {
                connect();
            }
        });
    }

    private void exit() {
        stop();
        setVisible(false);
    }

    public void connect() {
        try {
            int port = Integer.parseInt(this.serverPort.getText());
            chatClient.register(port);
            //name
            this.chatClient.setName(this.txtName.getText());
            this.txtName.setEditable(false);
        } catch (NumberFormatException ex) {
            receive("port should be a number");
        }

    }

    private void send() {
        String message = messageField.getText();
        this.messageField.setText("");
        chatClient.send(message);
    }

    private void stop() {
        chatClient.unregister();
    }

    public void receive(String text) {
        String historyText = history.getText();
        history.setText(historyText + '\n' + text);
    }
}
