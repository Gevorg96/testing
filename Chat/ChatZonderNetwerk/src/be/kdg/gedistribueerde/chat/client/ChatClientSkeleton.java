package be.kdg.gedistribueerde.chat.client;

import be.kdg.gedistribueerde.chat.client.frame.ChatFrame;
import be.kdg.gedistribueerde.chat.communication.MessageManager;
import be.kdg.gedistribueerde.chat.communication.MethodCallMessage;

import java.net.ConnectException;

public class ChatClientSkeleton {
    private ChatClient chatClient;
    private final ChatFrame chatFrame;
    private MessageManager messageManager;
    private ChatServerStub chatServerStub;

    public ChatClientSkeleton() {
        this.messageManager = new MessageManager(0);
        this.chatServerStub = new ChatServerStub(messageManager);
        this.chatClient = new ChatClient("by _-GS-_", chatServerStub);
        this.chatFrame = new ChatFrame(chatClient);
        waitForRequest();
    }

    public void waitForRequest() {
        MethodCallMessage request;
        while (true) {
            request = messageManager.wReceive();
            handleRequest(request);
        }
    }

    public void handleRequest(MethodCallMessage request) {
        String methodName = request.getMethodName();
        if ("message".equals(methodName)) {
            chatClient.receive(request.getParameter("message"));
        }
    }
}
