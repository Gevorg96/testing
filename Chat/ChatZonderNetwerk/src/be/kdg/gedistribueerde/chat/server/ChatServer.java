package be.kdg.gedistribueerde.chat.server;

import be.kdg.gedistribueerde.chat.client.ChatClient;

import java.util.List;
import java.util.ArrayList;

public class ChatServer {
    private List<ChatClientStub> clients;

    public ChatServer() {
        this.clients = new ArrayList<>();
    }

    public void register(ChatClientStub client) {
        if (this.clients.contains(client)){
            send("From Server", "you are allready connected to the server");
        }else{
            clients.add(client);
            System.out.println(client);
            send("From Server",  "someone has entered the room");
        }
    }

    public void unregister(ChatClientStub client) {
        clients.remove(client);
        send("From Server", " has left the room");
    }

    public void send(String name, String message) {
        for(ChatClientStub client : clients) {
            client.send(name + ": " + message);
        }
    }
}
