package be.kdg.gedistribueerde.chat.server;

import be.kdg.gedistribueerde.chat.communication.MessageManager;
import be.kdg.gedistribueerde.chat.communication.MethodCallMessage;


import java.net.ConnectException;

public class ServerSkeleton {
    private final MessageManager messageManager;
    private ChatServer chatServer;

    public ServerSkeleton() {
        this.chatServer = new ChatServer();
        this.messageManager = new MessageManager(666);
        System.out.println(messageManager.getMyAddress());
    }

    public void waitForRequest() {
        MethodCallMessage request;
        while (true) {
            request = messageManager.wReceive();
            handleRequest(request);
        }

    }

    public void handleRequest(MethodCallMessage request) {
        //System.out.println("ZipCodesSkeleton:handleRequest(" + request + ")");
        String methodName = request.getMethodName();
        if ("connect".equals(methodName)) {
            chatServer.register(new ChatClientStub(request.getOriginator(),messageManager));
            sendReplyOK(request);
        } else if ("message".equals(methodName)) {
            chatServer.send(""+request.getParameter("name"), request.getParameter("message"));
            sendReplyOK(request);
        } else {
            System.out.println(request);
        }
    }

    private void sendReplyOK(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        System.out.println(request);
        reply.setParameter("status", "Ok");
        reply.setParameter("result", "You are connected to the server");
        try {
            messageManager.send(reply, request.getOriginator());
        } catch (ConnectException e) {
            e.printStackTrace();
        }
    }

}
