package be.kdg.gedistribueerde.chat.server;

import be.kdg.gedistribueerde.chat.communication.MessageManager;
import be.kdg.gedistribueerde.chat.communication.MethodCallMessage;
import be.kdg.gedistribueerde.chat.communication.NetworkAddress;

import java.net.ConnectException;
import java.util.Objects;

public class ChatClientStub {
    NetworkAddress clientAdress;
    MessageManager messageManager;

    public ChatClientStub(NetworkAddress clientAdress, MessageManager servermessageManager) {
        this.clientAdress = clientAdress;
        this.messageManager = servermessageManager;
    }

    public void send(String message) {
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "message");
        methodCallMessage.setParameter("message", message);
        try {
            messageManager.send(methodCallMessage, clientAdress);
        } catch (ConnectException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChatClientStub)) return false;
        ChatClientStub that = (ChatClientStub) o;
        return Objects.equals(clientAdress, that.clientAdress) &&
                Objects.equals(messageManager, that.messageManager);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientAdress, messageManager);
    }
}
