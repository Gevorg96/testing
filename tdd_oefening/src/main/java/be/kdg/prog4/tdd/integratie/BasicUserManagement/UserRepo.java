package be.kdg.prog4.tdd.integratie.BasicUserManagement;

import be.kdg.prog4.tdd.integratie.BasicUserManagement.User;
import org.springframework.stereotype.Component;

import javax.jws.soap.SOAPBinding;
import java.util.*;

@Component
public class UserRepo {
    private Set<User> users;

    public UserRepo() {
        this.users = new HashSet<>();
    }

    public void addUser(String username, String password) {
        this.users.add(new User(username, password));
    }

    public boolean checkLogin(String username, String password) {
        return users.contains(new User(username, password));
    }

    public void removeUser(String username, String password) {
        this.users.remove(new User(username, password));
    }

    public List<String> getFavo(String username, String password) {
        for (Iterator<User> it = users.iterator(); it.hasNext(); ) {
            User u = it.next();
            if (u.naam.equals(username) && u.password.equals(password)) {
                return u.getFavorites();
            }
        }
        return new ArrayList<>();
    }

    public void addFavo(String username, String password, String favorite) {
        for (Iterator<User> it = users.iterator(); it.hasNext(); ) {
            User u = it.next();
            if (u.naam.equals(username) && u.password.equals(password)) {
                u.addFavorites(favorite);
            }
        }
    }


    public void removeFavo(String username, String password, String favorite){
        for (Iterator<User> it = users.iterator(); it.hasNext(); ) {
            User u = it.next();
            if (u.naam.equals(username) && u.password.equals(password)) {
                u.removeFavorites(favorite);
            }
        }
    }
}

