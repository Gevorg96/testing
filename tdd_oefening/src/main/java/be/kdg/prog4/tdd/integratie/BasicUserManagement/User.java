package be.kdg.prog4.tdd.integratie.BasicUserManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class User {
    String naam;
    String password;
    List<String> favorites;
    public User(String naam, String password) {
        this.naam = naam;
        this.password = password;
        favorites = new ArrayList<String>();
    }


    public List<String> getFavorites(){
        return favorites;
    }

    public void addFavorites(String favo) {
        this.favorites.add(favo);
    }
    public void removeFavorites(String favo) {
        this.favorites.remove(favo);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return naam.equals(user.naam) &&
                password.equals(user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(naam, password);
    }


}
