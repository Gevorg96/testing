package be.kdg.prog4.tdd.integratie.BasicUserManagement;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FavoriteService {

    UserRepo ur;

    public FavoriteService() {
        ur = new UserRepo();
        ur.addUser("root", "rootpasswd");
    }


    public void addUser(String root, String rootPass, String newUser, String userPass) {
        if (root.equals("root") && rootPass.equals("rootpasswd")) {
            ur.addUser(newUser, userPass);
        }
    }

    public void removeUser(String root, String rootPass, String user, String userPass) {
        if (root.equals("root") && rootPass.equals("rootpasswd")) {
            ur.removeUser(user, userPass);
        }
    }

    public boolean checkLogin(String user, String userPass) {
        return ur.checkLogin(user, userPass);
    }

    public List<String> getFavorites(String user, String userPass) {
        return ur.getFavo(user, userPass);
    }

    public void addFavorite(String user, String userPass, String favorite) {
        ur.addFavo(user, userPass, favorite);
    }
    public void removeFavorite(String user, String userPass, String favorite) {
        ur.removeFavo(user, userPass, favorite);
    }


}
